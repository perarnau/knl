#!/usr/bin/env python2

import pandas as pd
import sys

parse_file = sys.argv[1]+".parse"

frame = pd.read_csv(parse_file, delim_whitespace=True,
                    names=['mem', 'size', 'repeat', 'access', 'perf'])

grouped = frame.groupby(['mem', 'size'])

for sz in list(set(frame['size'])):
    slow = ('slow', sz)
    fast = ('fast', sz)
    gs = grouped.get_group(slow)
    gf = grouped.get_group(fast)
    print sz, gs.perf.mean(), gs.perf.std(), gf.perf.mean(), gf.perf.std(), gs.access.mean(), len(gs.index)
