#!/usr/bin/env python2

import pandas as pd
import sys

baseline_file = sys.argv[1]
parse_file = sys.argv[2]

bframe = pd.read_csv(baseline_file, delim_whitespace=True,
                    names=['work', 'threads', 'copy', 'perf'])
pframe = pd.read_csv(parse_file, delim_whitespace=True,
                    names=['work', 'threads', 'copy', 'perf'])

bg = bframe.groupby(['work', 'copy', 'threads'])
pg = pframe.groupby(['work', 'copy', 'threads'])

for copy in list(set(pframe['copy'])):
    for th in list(set(pframe['threads'])):
            for w in sorted(list(set(pframe['work']))):
                g = pg.get_group((w,copy, th))
                base = bg.get_group((w, 0, th - copy))
                speed = bg.get_group((w, 0, th))
                allw = bg.filter(lambda x: x.work.mean() == w)
                best = allw.groupby(['work', 'copy', 'threads']).mean().min().perf
                print w, g.perf.mean(), g.perf.std(), base.perf.mean(), base.perf.std(), speed.perf.mean(), speed.perf.std(), best, len(g.index)
