#!/usr/bin/env python2

import pandas as pd
import sys

baseline_file = sys.argv[1]
fastline_file = sys.argv[2]

bframe = pd.read_csv(baseline_file, delim_whitespace=True,
                    names=['work', 'threads', 'copy', 'perf'])
fframe = pd.read_csv(fastline_file, delim_whitespace=True,
                    names=['work', 'threads', 'copy', 'perf'])

bg = bframe.groupby(['work', 'threads'])
fg = fframe.groupby(['work', 'threads'])

for th in sorted(list(set(bframe['threads']))):
    ms = []
    l = list(set(bframe['work']))
    l.sort()
    for w in l:
        bgg = bg.get_group((w, th))
        fgg = fg.get_group((w, th))
        ms.append(bgg.perf.mean()/fgg.perf.mean())
    print th, " ".join(map(str, ms))
