#!/usr/bin/env python2

import pandas as pd
import sys

parse_file = sys.argv[1]

frame = pd.read_csv(parse_file, delim_whitespace=True,
                    names=['work', 'threads', 'copy', 'perf'])

grouped = frame.groupby(['work', 'threads'])

for th in sorted(list(set(frame['threads']))):
    ms = []
    l = list(set(frame['work']))
    l.sort()
    for w in l:
        g = grouped.get_group((w, th))
        ms.append(g.perf.mean())
    print th, " ".join(map(str, ms))
