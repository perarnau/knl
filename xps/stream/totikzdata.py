#!/usr/bin/env python2

import pandas as pd
import sys

parse_file = sys.argv[1]+".parse"

frame = pd.read_csv(parse_file, delim_whitespace=True,
                    names=['mem', 'threads', 'repeat', 'perf'])

grouped = frame.groupby(['mem', 'threads'])

for th in sorted(list(set(frame['threads']))):
    slow = ('slow', th)
    fast = ('fast', th)
    gs = grouped.get_group(slow)
    gf = grouped.get_group(fast)
    print th, gs.perf.mean(), gs.perf.std(), gf.perf.mean(), gf.perf.std(), len(gs.index)
