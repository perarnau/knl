#!/bin/sh
# make sure we stop on errors, and log everything
set -e
set -x
set -u
DATE=$(date +%Y%m%d.%H%M%S)
LOGFILE="$DATE.log"
DELIMITER="################################################################################"

# have stdout redirected to our LOGFILE
exec > "$LOGFILE"

# remember this script in the log
printf "%s\n%s\n\n" "$DELIMITER" "$DELIMITER"
cat "$0"
printf "\n%s\n" "$DELIMITER"

# Setup environment
echo "Start: $DATE"
export PATH=../../benchmarks/:$PATH

# Gather some info on the system
uname -a
cat /proc/cmdline
numactl --hardware
get-knl-modes
git describe --always
env 
printf "\n%s\n" "$DELIMITER"

FAST_MEM=4,5,6,7
SLOW_MEM=0,1,2,3
REPEATS=30

# record stream perf for both memory nodes, for varying amounts of threads
for th in 1 2 4 $(seq 8 4 64) $(seq 80 16 256)
do
	export OMP_NUM_THREADS=$th
	export OMP_PLACES=cores
	export OMP_PROC_BIND=spread
	for times in $(seq 1 $REPEATS);
	do
		echo "stream $th slow $times"
		numactl --cpunodebind $SLOW_MEM --membind $SLOW_MEM stream-membind

		echo "stream $th fast $times"
		numactl --cpunodebind $SLOW_MEM --membind $FAST_MEM stream-membind
	done
done

DONEDATE=$(date +%Y%m%d.%H%M%S)
echo "Done: $DONEDATE"
