#!/bin/sh
awk '/migrate / { type=$2; param=$3; times=$4 }
     /migrate:/ { print type, param, times, $6 }' $1
