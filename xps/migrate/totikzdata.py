#!/usr/bin/env python2

import pandas as pd
import sys
import scipy.stats
import math

def ci(std, num):
    # num -1 degrees of freedom
    student = scipy.stats.t.interval(.95, num -1)
    # student is a tuple, we want the positive value
    ratio = student[1]
    return (ratio*std)/math.sqrt(num)


parse_file = sys.argv[1]

frame = pd.read_csv(parse_file, delim_whitespace=True,
                    names=['type', 'param', 'repeat', 'time'])

grouped = frame.groupby(['param', 'type'])

for p in sorted(list(set(frame['param']))):
    gcp = grouped.get_group((p, 'memcpy'))
    cptm = gcp.time.mean()
    cpstd = gcp.time.std()
    try:
        gmv = grouped.get_group((p, 'move'))
        mvtm = gmv.time.mean()
        mvstd = gmv.time.std()
        print p, cptm, cpstd, ci(cpstd, len(gcp.time)), mvtm, mvstd, ci(mvstd, len(gmv.time)), len(gcp.index)
    except:
        print p, cptm, cpstd, ci(cpstd, len(gcp.time)), len(gcp.index)
