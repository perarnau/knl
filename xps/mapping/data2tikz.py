#!/usr/bin/env python2
from __future__ import print_function
import sys

prologue=r"""
\begin{tikzpicture}[scale=.5]
	
\coordinate (bl) at (0,0);
\coordinate (tr) at (16,8);
%\path[use as bounding box] (0,-2) grid (tr);

%TILES & CORES
\foreach \x in {0,2,4,...,14}
\foreach \y in {0,1,2,3}
{
	\draw[thick] (\x,\y) ++(.1,.1) rectangle +(1.8,.8);
	\draw (\x,\y) ++(.2,.2) rectangle +(.6,.6);
	\draw (\x,\y) ++(1,0) ++(.2,.2) rectangle +(.6,.6);
}

%QUADS
	\draw[very thick] (0,2) -- ++(16,0);
	\draw[very thick] (8,0) -- ++(0,4);
%MCDRAM
	\draw[very thick] (bl) ++(0,4) ++(.1,.1) rectangle ++(7.8,.5);
	\draw[very thick] (bl) ++(8,4) ++(.1,.1) rectangle ++(7.8,.5);
	\draw[very thick] (bl) ++(0,0) ++(.1,-.1) rectangle ++(7.8,-.5);
	\draw[very thick] (bl) ++(8,0) ++(.1,-.1) rectangle ++(7.8,-.5);
%DDR
	\draw[very thick] (bl) ++(0,4.7) ++(.1,.1) rectangle ++(7.8,.5);
	\draw[very thick] (bl) ++(8,4.7) ++(.1,.1) rectangle ++(7.8,.5);
	\draw[very thick] (bl) ++(0,-.7) ++(.1,-.1) rectangle ++(7.8,-.5);
	\draw[very thick] (bl) ++(8,-.7) ++(.1,-.1) rectangle ++(7.8,-.5);
% TEXT
	\path (bl) ++(4,4.35) node{\tiny MCDRAM 0};
	\path (bl) ++(12,4.35) node{\tiny MCDRAM 1};
	\path (bl) ++(4,-.35) node{\tiny MCDRAM 2};
	\path (bl) ++(12,-.35) node{\tiny MCDRAM 3};
	\path (bl) ++(4,5.05) node{\tiny DDRAM 0};
	\path (bl) ++(12,5.05) node{\tiny DDRAM 1};
	\path (bl) ++(4,-1.05) node{\tiny DDRAM 2};
	\path (bl) ++(12,-1.05) node{\tiny DDRAM 3};
"""

epilogue=r"""
\end{tikzpicture}
"""

def core2quad(core):
    return (core % 64) // 16

def quad2base(quad):
    x = (quad % 2)
    y = (quad // 2)
    return (x,-y)

def draw_tid(tid, core):
    quad = core2quad(core)
    quad_x, quad_y = quad2base(quad)
    quad_core = core % 16

    quad_x = quad_x * 4
    quad_y = quad_y * 2

    core_x = quad_core % 8
    core_y = -(quad_core // 8)

    x = quad_x * 2 + core_x
    y = quad_y + core_y
    print("\path (0.5,3.5) ++(%d,%d) node {\\tiny %d};" % (x,y,tid) ) 

print(prologue)
parse_file = sys.argv[1]
with open(parse_file,'r') as f:
    for line in f:
        places, bind, th, tid, core = line.split()
        draw_tid(int(tid), int(core))
print(epilogue)
