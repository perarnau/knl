#!/bin/sh
# make sure we stop on errors, and log everything
set -e
set -x
set -u
DATE=$(date +%Y%m%d.%H%M%S)
LOGFILE="$DATE.log"
DELIMITER="################################################################################"

# have stdout redirected to our LOGFILE
exec > "$LOGFILE"

# remember this script in the log
printf "%s\n%s\n\n" "$DELIMITER" "$DELIMITER"
cat "$0"
printf "\n%s\n" "$DELIMITER"

# Setup environment
echo "Start: $DATE"
export PATH=../../benchmarks/:$PATH

# Gather some info on the system
uname -a
cat /proc/cmdline
numactl --hardware
env 
printf "\n%s\n" "$DELIMITER"

# Deal with system limits
ulimit -n 4096
ulimit -a

printf "\n%s\n" "$DELIMITER"

# record thread mapping for all modes
th=64
for places in threads cores sockets
do
	for bind in true master close spread
	do
		export OMP_NUM_THREADS=$th
		export OMP_PLACES=$places
		export OMP_PROC_BIND=$bind
		printf "mapping %s %s %s\n" "$th" "$places" "$bind"
		mapping
	done
done

DONEDATE=$(date +%Y%m%d.%H%M%S)
echo "Done: $DONEDATE"
