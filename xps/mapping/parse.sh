#!/bin/sh
awk '/mapping/ { th=$2; places=$3; bind=$4 }
     /tid:/ { print places, bind, th, $2, $4 }' "$1"
