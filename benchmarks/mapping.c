#define _GNU_SOURCE
#include <omp.h>
#include <sched.h>
#include <stdio.h>

int main(void)
{

#pragma omp parallel
	{
		int mytid = omp_get_thread_num();
		int mycpu = sched_getcpu();
		printf("tid: %u cpu: %u\n", mytid, mycpu);
	}
	return 0;
}
