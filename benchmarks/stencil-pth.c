#include <assert.h>
#include <errno.h>
#include <numa.h>
#include <numaif.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <hwloc.h>

//#define STENCIL_PROFILE
#define MAX_NUMNODES 64

/*******************************************************************************
 * global variables
 ******************************************************************************/

char *sdesc;
char *ddesc;
unsigned int numthreads, copythreads, iterations, density;
size_t slogsize, smemsize, ssize, flogsize, fmemsize, fsize;
size_t tilesize;
size_t ghostcells = 1;
struct bitmask *smask, *dmask;
unsigned long *slow, *fast;
pthread_barrier_t startbarrier, loopbarrier, endbarrier;

#ifdef STENCIL_PROFILE
/* thread specific timings */
long long int *itertimes;
long long int *waittimes;

void clockwait_start(struct timespec *start, struct timespec *stop, unsigned int tid)
{
	clock_gettime(CLOCK_REALTIME, start);
	waittimes[tid] += (start->tv_nsec - stop->tv_nsec) + 1e9* (start->tv_sec - stop->tv_sec);
}

void clockwait_stop(struct timespec *start, struct timespec *stop, unsigned int tid)
{
	clock_gettime(CLOCK_REALTIME, stop);
	itertimes[tid] += (stop->tv_nsec - start->tv_nsec) + 1e9* (stop->tv_sec - start->tv_sec);
}

void clockwait_stop_nop(struct timespec *start, struct timespec *stop, unsigned int tid)
{
	clock_gettime(CLOCK_REALTIME, stop);
}

void clockwait_print(unsigned int tid)
{
	printf("clockwait: tid: %u iter %lld wait %lld\n", tid, itertimes[tid],
	       waittimes[tid]);
}
#endif

/*******************************************************************************
 * hwloc support
 ******************************************************************************/

hwloc_topology_t topology;
hwloc_obj_t *roots;
unsigned int nroots;
hwloc_cpuset_t *cpusets;
int *maskmap;

void myaffinity(unsigned long tid)
{
	hwloc_bitmap_singlify(cpusets[maskmap[tid]]);
	hwloc_set_cpubind(topology, cpusets[maskmap[tid]], HWLOC_CPUBIND_THREAD);
	printf("myaffinity: tid: %lu @ CPU %u\n", tid, sched_getcpu());
}

/*******************************************************************************
 * pthreads functions
 ******************************************************************************/

/* perform chunked read from contiguous source (slow) memory to ghosted
 * destination buffer (in fast memory).
 * blocksize data items are read/written, with ghostcell overlapping between
 * source blocks.
 */
void do_chunk_s2f(unsigned long tid, unsigned long nbthreads,
		  size_t srcid, size_t dstid, size_t blocksize, size_t realsize)
{
	size_t dststart = dstid * blocksize;
	size_t worksize = blocksize - 2*ghostcells;
	size_t srcstart = (srcid * worksize) - ghostcells;

	size_t total = realsize + 2*ghostcells;
	size_t chunksize = total/nbthreads;
	size_t dstidx = dststart + tid * chunksize;
	size_t srcidx = srcstart + tid * chunksize;

	/* make sure the last thread deals with reminder data */
	if(tid == nbthreads - 1 && total > chunksize * nbthreads)
		chunksize += total % nbthreads;

	memcpy(&fast[dstidx], &slow[srcidx], chunksize*sizeof(unsigned long));
}

void *th_s2f(void *arg)
{
	unsigned long i = (unsigned long)arg;
#ifdef STENCIL_PROFILE
	struct timespec start, stop;
#endif
	myaffinity(i);
	size_t zonesize = tilesize;
	size_t worksize = tilesize-2;
	size_t nbthreads = copythreads/2;
	pthread_barrier_wait(&startbarrier);
	for(unsigned long iter = 0; iter < iterations; iter++)
	{
#ifdef STENCIL_PROFILE
		clockwait_stop_nop(&start, &stop, i);
#endif
		unsigned long read = 0;
		for(unsigned long sidx = 0; sidx*worksize < ssize; sidx++)
		{
#ifdef STENCIL_PROFILE
			clockwait_start(&start, &stop, i);
#endif
			size_t sz = worksize;
			/* handle the last chunk */
			if(ssize - sidx*worksize < worksize)
				sz = ssize - sidx*worksize;
			do_chunk_s2f(i, nbthreads, sidx, read, zonesize, sz);
#ifdef STENCIL_PROFILE
			clockwait_stop(&start, &stop, i);
#endif
			pthread_barrier_wait(&loopbarrier);
			read = (read +1) % 2;
		}
		/* wait for last compute */
		pthread_barrier_wait(&loopbarrier);
		/* wait for last write back */
		pthread_barrier_wait(&loopbarrier);
#ifdef STENCIL_PROFILE
		clockwait_start(&start, &stop, i);
#endif
	}
	pthread_barrier_wait(&endbarrier);
	return arg;
}

/* perform chunked read from ghosted data (in fast memory) to destination
 * storage (slow memory).
 * worksize data is read/written, as ghostcells are not copied back.
 */
void do_chunk_f2s(unsigned long tid, unsigned long nbthreads,
		  size_t srcid, size_t dstid, size_t blocksize, size_t realsize)
{
	size_t srcstart = srcid * blocksize;
	size_t worksize = blocksize - 2*ghostcells;
	size_t dststart = dstid * worksize;

	size_t total = realsize + 2*ghostcells;
	size_t chunksize = total/nbthreads;
	size_t srcidx = srcstart + tid * chunksize;
	size_t dstidx = dststart + tid * chunksize;

	/* make sure the first thread doesn't write back the ghost cells */
	if(tid == 0)
	{
		srcidx += ghostcells;
		chunksize -= ghostcells;
	}

	/* make sure the last thread doesn't copy too much */
	if(tid == nbthreads -1 && total > chunksize * nbthreads)
		chunksize = chunksize - ghostcells + (total % nbthreads);

	memcpy(&slow[dstidx], &fast[srcidx], chunksize*sizeof(unsigned long));
}


void *th_f2s(void *arg)
{
	unsigned long i = (unsigned long)arg;
#ifdef STENCIL_PROFILE
	struct timespec start, stop;
#endif
	myaffinity(i+copythreads/2);
	size_t zonesize = tilesize;
	size_t worksize = tilesize -2;
	size_t nbthreads = copythreads/2;
	pthread_barrier_wait(&startbarrier);
	for(unsigned long iter = 0; iter < iterations; iter++)
	{
		unsigned long write = 2;
#ifdef STENCIL_PROFILE
		clockwait_stop_nop(&start, &stop, i+copythreads/2);
#endif
		/* wait for first tile to be read, and worked on */
		pthread_barrier_wait(&loopbarrier);
		pthread_barrier_wait(&loopbarrier);
		for(unsigned long sidx = 0; sidx*worksize < ssize; sidx++)
		{
#ifdef STENCIL_PROFILE
			clockwait_start(&start, &stop, i+copythreads/2);
#endif
			size_t sz = worksize;
			/* handle the last chunk */
			if(ssize - sidx*worksize < worksize)
				sz = ssize - sidx*worksize;
			do_chunk_f2s(i, nbthreads, write, sidx, zonesize, sz);
#ifdef STENCIL_PROFILE
			clockwait_stop(&start, &stop, i+copythreads/2);
#endif
			pthread_barrier_wait(&loopbarrier);
			write = 2 + (write +1) % 2;
		}
#ifdef STENCIL_PROFILE
		clockwait_start(&start, &stop, i+copythreads/2);
#endif
	}
	pthread_barrier_wait(&endbarrier);
	return arg;
}

void do_chunk_kernel(unsigned long tid, unsigned long nbthreads,
		size_t srcid, size_t dstid, size_t blocksize, size_t realsize)
{
	size_t srcstart = srcid * blocksize;
	size_t dststart = dstid * blocksize;

	size_t total = realsize + 2*ghostcells;
	size_t chunksize = total/nbthreads;
	size_t srcidx = srcstart + tid * chunksize;
	size_t dstidx = dststart + tid * chunksize;

	size_t start = 0;
	/* make sure the first thread doesn't write into ghostcells */
	if(tid == 0)
	{
		start += ghostcells;
		chunksize -= ghostcells;
	}

	/* make sure the last thread work until the end */
	if(tid == nbthreads -1 && total > chunksize * nbthreads)
		chunksize = chunksize - ghostcells + (total % nbthreads);

	for(unsigned int r = 0; r < density; r++)
	{
		for(size_t i = start; i < chunksize; i++)
			fast[dstidx+i] = fast[srcidx+i-1] + fast[srcidx+i]
				       + fast[srcidx+i+1];
	}
}

void *th_walk(void *arg)
{
	unsigned long i = (unsigned long)arg;
#ifdef STENCIL_PROFILE
	struct timespec start, stop;
#endif
	myaffinity(i+copythreads);
	size_t zonesize = tilesize;
	size_t worksize = tilesize-2;
	size_t nbthreads = numthreads - copythreads;
	pthread_barrier_wait(&startbarrier);
	for(unsigned long iter = 0; iter < iterations; iter++)
	{
		unsigned long calc, temp;
		calc = 0; temp = 2;
		/* wait for first tile to arrive */
#ifdef STENCIL_PROFILE
		clockwait_stop_nop(&start, &stop, i+copythreads);
#endif
		pthread_barrier_wait(&loopbarrier);
		/* work until the entire array has been processed */
		for(unsigned long sidx = 0; sidx*worksize < ssize; sidx++)
		{
#ifdef STENCIL_PROFILE
			clockwait_start(&start, &stop, i+copythreads);
#endif
			size_t sz = worksize;
			/* handle the last chunk */
			if(ssize - sidx*worksize < worksize)
				sz = ssize - sidx*worksize;
			do_chunk_kernel(i, nbthreads, calc, temp, zonesize, sz);
#ifdef STENCIL_PROFILE
			clockwait_stop(&start, &stop, i+copythreads);
#endif
			pthread_barrier_wait(&loopbarrier);
			calc = (calc +1) % 2;
			temp = 2 + (temp +1) % 2;
		}
		/* wait for write to finish */
		pthread_barrier_wait(&loopbarrier);
#ifdef STENCIL_PROFILE
		clockwait_start(&start, &stop, i+copythreads);
#endif
	}
	pthread_barrier_wait(&endbarrier);
	return arg;
}

/*******************************************************************************
 * main/setup/teardown
 ******************************************************************************/

int main(int argc, char *argv[])
{
	int err;
	unsigned int i;
	hwloc_cpuset_t hwloc_task_affinity;
	assert(argc == 9);
	sdesc = argv[1];
	ddesc = argv[2];
	slogsize = atoi(argv[3]);
	flogsize = atoi(argv[4]);
	smemsize = 1UL << slogsize;
	fmemsize = 1UL << flogsize;
	ssize = smemsize/sizeof(unsigned long);
	fsize = fmemsize/sizeof(unsigned long);
	numthreads = atoi(argv[5]);
	copythreads = atoi(argv[6]);
	tilesize = fsize/4;
	/* make sure the last chunk is at least a bit big */
	if(ssize % (tilesize-2) < numthreads + 2*ghostcells)
	{
		unsigned long trim = ssize % (tilesize-2);
		printf("warning: trimming slow size by %lu elements avoid chunking issues\n", trim);
		ssize -= trim;
	}
	iterations = atoi(argv[7]);
	density = atoi(argv[8]);
	smask = numa_parse_nodestring_all(sdesc);
	dmask = numa_parse_nodestring_all(ddesc);

	/* topology: see hwloc-distrib source code for how to do that */
	hwloc_topology_init(&topology);
	hwloc_topology_load(topology);
	nroots = hwloc_get_nbobjs_by_depth(topology, 0);
	roots = malloc(nroots*sizeof(hwloc_obj_t));
	for(i = 0; i < nroots; i++)
		roots[i] = hwloc_get_obj_by_depth(topology, 0, i);
	cpusets = malloc(numthreads*sizeof(hwloc_bitmap_t));
	maskmap = calloc(numthreads,sizeof(int));

	/* restrict topology to allowed CPUs */
	hwloc_task_affinity = hwloc_bitmap_alloc();
	hwloc_get_cpubind(topology, hwloc_task_affinity, HWLOC_CPUBIND_STRICT);
	hwloc_topology_restrict(topology, hwloc_task_affinity, 0);

	hwloc_distrib(topology, roots, nroots, cpusets, numthreads, INT_MAX, 0);

	/* we need to interleave copythreads */
	for(i = 0; i < numthreads; i++)
		maskmap[i] = i;
	for(i = 0; i < copythreads; i++)
	{
		int dst = i*(numthreads/copythreads);
		int tmp = maskmap[dst];
		maskmap[dst] = i;
		maskmap[i] = tmp;
	}

	if (set_mempolicy(MPOL_INTERLEAVE, smask->maskp, sizeof(*smask->maskp) * 8) < 0) {
		fprintf(stderr, "error: setting memory policy for slow mem\n");
		exit(EXIT_FAILURE);
	}

	/* add ghostcells to slow */
	slow = mmap(NULL, smemsize + 2*ghostcells*sizeof(unsigned long), PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
	assert(slow != MAP_FAILED);

	slow = (void*)((char *)slow + (ghostcells * sizeof(unsigned long)));
	for(unsigned long i = 0; i < ssize; i++)
		slow[i] = i;
	slow[-1] = 0;
	slow[ssize] = ssize;

	if (set_mempolicy(MPOL_DEFAULT, NULL, 0) < 0) {
		fprintf(stderr, "error: setting default memory policy\n");
		exit(EXIT_FAILURE);
	}

	if (set_mempolicy(MPOL_INTERLEAVE, dmask->maskp, sizeof(*dmask->maskp) * 8) < 0) {
		fprintf(stderr, "error: setting memory policy for fast mem\n");
		exit(EXIT_FAILURE);
	}

	fast = mmap(NULL, fmemsize, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
	assert(fast != MAP_FAILED);

	for(unsigned long i = 0; i < fsize; i++)
		fast[i] = i;

	struct timespec start, stop;
	unsigned long curth = 0;
	void *ret;

#ifdef STENCIL_PROFILE
	itertimes = calloc(numthreads, sizeof(*itertimes));
	waittimes = calloc(numthreads, sizeof(*waittimes));
#endif

	pthread_t *ths = calloc(numthreads, sizeof(pthread_t));
	pthread_barrier_init(&startbarrier, NULL, numthreads+1);
	pthread_barrier_init(&endbarrier, NULL, numthreads+1);
	pthread_barrier_init(&loopbarrier, NULL, numthreads);
	for(unsigned long j = 0; j < copythreads/2; j++)
	{
		pthread_create(&ths[curth], NULL, &th_s2f, (void*)j);
		curth++;
	}
	for(unsigned long j = 0; j < copythreads/2; j++)
	{
		pthread_create(&ths[curth], NULL, &th_f2s, (void*)j);
		curth++;
	}
	for(unsigned long j = 0; j < numthreads - copythreads; j++)
	{
		pthread_create(&ths[curth], NULL, &th_walk, (void*)j);
		curth++;
	}

	pthread_barrier_wait(&startbarrier);
	clock_gettime(CLOCK_REALTIME, &start);
	pthread_barrier_wait(&endbarrier);
	clock_gettime(CLOCK_REALTIME, &stop);

	for(unsigned long j = 0; j < numthreads; j++)
		pthread_join(ths[j], &ret);

#ifdef STENCIL_PROFILE
	for(unsigned long j = 0; j < numthreads; j++)
		clockwait_print(j);
#endif

	long long int time_nano=0;
	time_nano = (stop.tv_nsec - start.tv_nsec) +
		1e9* (stop.tv_sec - start.tv_sec);
	printf("stencil: %zd %zd %u %u %u %u %lld\n", ssize, fsize, numthreads, copythreads, iterations, density, time_nano);
	free(ths);

#ifdef STENCIL_PROFILE
	free(itertimes);
	free(waittimes);
#endif
	munmap(slow - ghostcells, smemsize + 2*ghostcells*sizeof(unsigned long));
	munmap(fast, fmemsize);
	numa_free_nodemask(smask);
	numa_free_nodemask(dmask);
	return 0;
}
