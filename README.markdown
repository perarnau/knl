# Work Distribution, Core and Memory binding on Intel's Knight Landing Architecture

This repository contains our efforts to understand at the deepest level the
impact of work distribution patterns and Operating System bindings on the
performance of High Performance Computing applications.

Making as little assumptions as possible, we aim to uncover for all the modes
of the KNL (cluster modes and memory modes) the best memory allocation and
thread binding policies, for a wide range of typical parallel work distribution
and memory access patterns.

## Repository Organization

* `benchmarks` contains the codes we use for all our experiments.
* `xps` contains the results (*.log*) files, as well as parsers and data
generators.

* `notebook.tex` contains a full description of all experiments as well as the
figures extracted from them.

* `paper.tex` is the extracted technical report/paper containing the information
we uncovered in a digestible manner (i.e. only the relevant experiment are
displayed), as well as our commentary.
