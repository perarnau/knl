#
# $Id: Makefile 279 2008-01-27 16:19:38Z balaji $
#
# Copyright (C) 2006-2007, University of Chicago. All rights reserved.
#

HEADER = paper
BIB = ./
TEX = ./
XPS = ./xps

TARGETS: xps paper.pdf
.PHONY: all clean xps

xps_files = $(shell find $(XPS) -name Makefile -print)

all: xps paper.pdf notebook.pdf

xps: $(xps_files)
	@for f in $(xps_files); \
	do \
		dir=`dirname $$f`; \
		make -C $$dir; \
	done

%.pdf: %.tex xps 
	TEXMFOUTPUT=`pwd` rubber -d $*

xpsclean:
	@for f in $(xps_files); \
	do \
		dir=`dirname $$f`; \
		make -C $$dir clean;\
	done

clean: xpsclean
	rubber -d --clean paper ; \
	rubber -d --clean notebook ;
