\documentclass[conference]{IEEEtran}
\usepackage{mathptmx}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[american]{babel}
\usepackage[pdftex]{graphicx}
\usepackage{fixltx2e}
\usepackage{url}
\usepackage{tikz}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepackage{enumitem}
\usepackage{cite}
\usepackage{subcaption}
\usepackage{listings}

% Compute terms that LaTeX hyphenates badly by default.
\hyphenation{off-loaded peta-scale exa-scale time-stamp light-weight in-fra-struc-ture}

% Bring figures closer to text
\renewcommand{\textfloatsep}{\intextsep}
\renewcommand{\dbltextfloatsep}{\intextsep}

\setdescription{topsep=0pt,partopsep=0pt,itemsep=0pt,parsep=0pt,leftmargin=\parindent}
\renewcommand{\floatpagefraction}{0.9}

\begin{document}

\title{Work Distribution, Memory Policies, and Thread Binding Strategies for
Intel's Knights Landing Architecture}

\maketitle

\section*{Introduction}

Intel's Knights Landing architecture is a recent and popular many-core platform
for High Performance Computing. The United States' Department of Energy in
particular is expected to put into production several machines comprised of
this architecture or its next generations: Theta (and the future Aurora) at
Argonne National Laboratory for example.

On top of being a many-core architecture, the Knights Landing also contains
on-package high bandwidth memory called MCDRAM\@: an additional layer in the
memory hierarchy that exchange a smaller capacity for a higher bandwidth. At
boot time, each system can also be configured into a different set of modes
regarding caching and memory behavior. From a topology standpoint, such
platform is thus complex to reason about and optimize, different parallel
programming patterns ending up with different optimal memory and thread
placement policies.

The aim of this report is to provide as much information as possible on
valuable strategies available to users, on the consequences of typical 
options and modes on performance as well as guidelines for the optimization
or design of new parallel work decomposition schemes. This report relies
heavily on actual experiments performed on the target platform, and strive to
provide as much details as possible in its analyses and the underlying
assumptions it makes. The repository containing this report should also
provides complete access to the logs, source code and scripts used in the
report.

\section{Architecture}

The Intel Knights Landing is a many-core processor available today under the
Xeon Phi brand. It is the third generation of many-cores from Intel, after the
Knights Ferry first generation prototype and the Knights Corner coprocessor. It
is the first generation to be available as a single standalone processor
package (no other processor is necessary to run a machine), although a
co-processor version also exists. Note that most of the information in this
section is explained in details in the official book.

%TODO: cite book

The layout of the Knights Landing is comprised of 38 \emph{tiles}, connected
through a network-on-chip. Each tile consists of two cores, each with private
dL1 and iL1 caches, and a shared 16-way associative 1 MiB unified L2 cache.
While there are 38 tiles on the package, the actual number of tiles available
to a user is always smaller. Depending on the exact model of Knights Landing,
between 2 and 6 tiles are disabled when the processor leaves the factory. Those
\emph{extra} tiles are just here to improve manufacturing yields. Thus,
publicly available processors contain between 64 and 72 cores. Each core is
capable of 4-way hyperthreading, resulting in between 256 and 288 CPUs visible
at the operating system level.

%TODO: block diagram of Knights Landing from intel

A on-chip network or \emph{mesh} links together the tiles to support access to
data stored in all L2 caches, as well as access to the two layers of memory
(MCDRAM and DDR). This 2D interconnect maintains cache coherency using the
MESIF protocol. Cache lines present in L1 and L2 are tracked using a
distributed tag directory, with each tile maintaining its portion in a
caching/home agent (CHA) box. The mesh can be configured to handle cache line
requests differently depending on a boot-time \emph{cluster mode} configuration:

\begin{itemize}
	\item \textbf{all-to-all}: physical addresses are interleaved uniformly
		across all devices of a memory layer. A hash function distribute
		uniformly cache line tracking among CHA boxes, without tacking
		into account the physical memory distribution.
	\item \textbf{quadrant}: the topology is divided into four virtual
		clusters. Memory interleaving is the same as for all-to-all,
		but the hashing function ensures that cache lines are managed
		by a CHA box in the same quadrant as the physical memory
		location.
	\item \textbf{sub-NUMA clustering}: the clusters are exposed to the
		users. Each cluster receives a contiguous physical memory
		range, and addresses are interleaved among devices of the same
		cluster. Same cache line tracking as quadrant mode.
\end{itemize}

Extra modes named \textbf{hemisphere} and \textbf{snc2} use two virtual
clusters instead of four. The goal of all those cluster modes are to offer a
trade-off between the length of the on-chip communication for a memory request
and the complexity of the topology exposed to the user. Assuming contiguous
accesses in physical memory, the all-to-all mode will result in memory requests
traveling on average longer across the on-chip network than in quadrant or SNC
mode, resulting mostly in bandwidth degradation. The quadrant mode will ensure
a faster communications between CHA and memory controller, but will not reduce
by much the on-chip traffic. The SNC mode is the most performant of all, but
requires that users deal with NUMA memory allocation policies to ensure the
best performance.

As we said before, the Knights Landing processor has access to two types of
memory: MCDRAM and DDR\@. MCDRAM is a high bandwidth, low capacity on-package
memory. Eight 2 GiB MCDRAM blocks are integrated to the Knights Landing, for a
total of 16 GiB. For DDR, two DDR4 memory controlled are on opposite sides of
the chip, each with 3 channels available. As each channel supports one DIMM,
the maximum DDR capacity is of 384 GiB. The memory can be configured at boot
time in three different modes:

\begin{itemize}
	\item \textbf{cache mode}: MCDRAM acts as a direct-mapped cache for the DDR\@;
	\item \textbf{flat mode}: MCDRAM acts like regular memory and shares the address 
		space of DDR\@;
	\item \textbf{hybrid mode}: where a portion acts like cache and the other like
		memory.
\end{itemize}

Both the cluster and memory modes interact with each other. For example, a
node booted in cache/quadrant will only see a single physical memory, the size
of the available DDR and all memory accesses will go through MCDRAM (acting as
a cache). On the opposite, a flat/snc4 mode will exhibit 8 NUMA memory nodes,
4 for the MCDRAM, 4 for the DRAM\@. Note the ACPI table reported by the processor
in those cases ensure that the operating system will not allocate anything in
MCDRAM unless specifically asked to.

\section{Memory and Threads Binding Interfaces}

As different boot-time parameters result in different hardware topologies (and
slight performance differences) being exposed to the user, one most take great
care in how to map a parallel program onto a Knights Landing processor.  On
Linux, the system provides various interfaces to perform this mapping, both for
memory and threads.

\subsection{Inspecting the Current Configuration}

Before any mapping can be decided, we first look at methods to read the current
topology and configuration of any KNL node.

While the boot-time parameters cannot be accessed on a running machine, the
resulting configuration of cluster and memory modes has a direct impact on the
topology read by Linux from the ACPI table. This topology can be read directly
from the system files (\texttt{/sys} and \texttt{/proc}) or, more easily from
the usual tools like \texttt{numactl} or \texttt{hwloc}.

Hwloc~\cite{hwloc} is a library and collection of binaries to inspect the
topology of any system. First developed as a mean to help the mapping of MPI
programs onto NUMA machines, it is known widely used to inspect
programmatically the topology of any system and to bind memory or threads to
specific parts of any architecture. Intel improved the library to also be
gather information on current cluster and memory modes. One can access this
information from inside the program using the code listing~\ref{fig:code:modes},
from the hwloc documentation.

\begin{figure}
\scriptsize
\lstinputlisting[language=C,tabsize=2,firstline=11]{benchmarks/get-knl-modes.c}
\caption{Using Hwloc to retrieve KNL modes.}
\label{fig:code:modes}
\end{figure}

Hwloc can also be used to inspect the NUMA topology reported by the operating
system, using \texttt{lstopo}, \texttt{hwloc-info} or using the C library
directly.

\subsection{NUMA, Hugepages, and Memory Policies}




\subsection{Thread Bindings: Native and OpenMP Interfaces}

\clearpage

\begin{figure}
\input{xps/mapping/dap_flatsnc416_cores_spread}
\caption{\small THREADS=16, OMP\_PLACES=cores, OMP\_PROC\_BIND=spread}
\end{figure}

\begin{figure}
\input{xps/mapping/dap_flatsnc416_cores_close}
\caption{\small THREADS=16, OMP\_PLACES=cores, OMP\_PROC\_BIND=close}
\end{figure}

\begin{figure}
\input{xps/mapping/dap_flatsnc416_sockets_spread}
\caption{\small THREADS=16, OMP\_PLACES=sockets, OMP\_PROC\_BIND=spread}
\end{figure}

\begin{figure}
\input{xps/mapping/dap_flatsnc464_cores_spread}
\caption{\small THREADS=64, OMP\_PLACES=cores, OMP\_PROC\_BIND=spread}
\end{figure}

\begin{figure}
\input{xps/mapping/dap_flatsnc432_cores_spread}
\caption{\small THREADS=32, OMP\_PLACES=cores, OMP\_PROC\_BIND=spread}
\end{figure}

\begin{figure}
\input{xps/mapping/dap_flatsnc432_cores_close}
\caption{\small THREADS=32, OMP\_PLACES=cores, OMP\_PROC\_BIND=close}
\end{figure}

\begin{figure}
\input{xps/mapping/dap_flatsnc432_sockets_spread}
\caption{\small THREADS=32, OMP\_PLACES=sockets, OMP\_PROC\_BIND=spread}
\end{figure}

\begin{figure}
\input{xps/mapping/dap_flatsnc464_sockets_spread}
\caption{\small THREADS=64, OMP\_PLACES=sockets, OMP\_PROC\_BIND=spread}
\end{figure}

\clearpage

\section{Basic Performance Figures}

%\section{Tuning and Optimization Methods}

\end{document}
